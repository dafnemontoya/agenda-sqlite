package com.example.agendasqlite;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import database.AgendaContacto;
import database.Contacto;

public class ListActivity extends AppCompatActivity {
    private AgendaContacto agendaContacto;
    private Button btnNuevo;
    private ArrayList<Contacto> listaContacto;
    private MyArrayAdapter adapter;
    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        agendaContacto = new AgendaContacto(this);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        lista = (ListView) findViewById(R.id.list);

        llenarLista();
        adapter = new MyArrayAdapter(this, R.id.lblNombre, R.layout.layout_contacto, listaContacto);
        lista.setAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void llenarLista() {
        agendaContacto.openDatabase();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrar();
    }


    //Clase interna
    class MyArrayAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;
        private LayoutInflater layoutInflater;

        public MyArrayAdapter(@NonNull Context context, @IdRes int idTextView, @LayoutRes int layoutIdResource, ArrayList<Contacto> contactos) {
                super(context, idTextView, contactos);
                this.context = context;
                this.textViewResourceId = layoutIdResource;
                this.contactos = contactos;
                this.layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombre);
            TextView lblTelefono = (TextView) view.findViewById(R.id.lblTel1);
            Button btnModificar = (Button) view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button) view.findViewById(R.id.btnBorrar);

            if (contactos.get(position).getFavorito() > 0) {
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }
            else {
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }

            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContacto.openDatabase();
                    agendaContacto.eliminarContacto(contactos.get(position).getId());
                    agendaContacto.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            return view;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            return getView(position, convertView, parent);
        }

    } // fin de clase MyArrayAdapter

}
